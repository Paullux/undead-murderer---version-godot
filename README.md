# Undead Murderer
  
![Couverture](https://framagit.org/Paullux/Undead_Murderer/raw/master/Images/ZombiesInNight.png)  
  
Le projet a pour but de construire un jeu FPS libre contre des Zombies (en voici le [scenario](https://sourceforge.net/p/undead-murderer/wiki/Scenario/)).  
D'un côté je créer des modèles 3D avec blender (relativement simple, je débute en Blender).  
De l'autre j'incorpore ces modèles dans Godot Engine et j'écrit des script pour contrôler le jeu et le joueur.  

## Pour commencer

Vous pouvez récupéré les sources avec la commande git suivante :
```
git clone https://framagit.org/Paullux/undead-murderer---version-godot.git 
```  
  
https://framagit.org/Paullux/undead-murderer---version-godot.git  
  
Si vous voulez participer, contactez moi.  

### Outils de développement

Pour développer le jeu, on utilise les logiciels suivants :

```
Blender  
Krita  
GIMP  
MakeHuman  
Godot Engine
```  

### Installation

J'ai compilé 4 versions du jeu :  
**linux 64 bits**   
**linux 32 bits** 
**windows**   
**macOS**

**Toutes téléchargeables ici :** 
  
[![Download Undead Murderer](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/undead-murderer/files/latest/download)
    
  [![Download Undead Murderer](https://img.shields.io/sourceforge/dt/undead-murderer.svg)](https://sourceforge.net/projects/undead-murderer/files/latest/download)
    
Télécharger la version pour votre système.  
  
Puis :  
**pour Linux 64 bits** *vous aurez un appimage.*  
**pour Linux 32 bits** *décompressez l'archive en mettant les éléments de l'archive dans le même dossier, puis ouvrez le fichier binaire (exécutable).*  
**pour Windows** *vous aurez accès à un setup classique suivez les instructions pour installer le jeu.*  
**pour macOS** *faites glisser le dmg dans votre dossier "Applications", s'il est bloqué par "sécurité" allez dans les paramètres de sécurité pour autorisé l'ouverture du jeu, après le jeu sera dans vos applications.*  
  
**Si l'installation ou l'exécution sont bloquées** je vous invite à vous rendre sur cette page [blocage](https://sourceforge.net/p/undead-murderer/wiki/Security%20Apple%20or%20Microsoft/)  
  
    
## Authors

* **Paul WOISARD** - *Initial work* - [mon profil framagit Paullux](https://framagit.org/Paullux)  
  
* **Discord pour le Jeu** - *pour communiquer* - [Lien par ici](https://discord.gg/udp9vKe)

Voir aussi la liste des [contributeurs](https://framagit.org/Paullux/Undead_Murderer/project_members) qui participent au project.  

## Licence

Le projet est sous licence GNU/GPLv3 pour tout ce qui est le code - voir le fichier [LICENSE.md](LICENSE.md) pour plus de détailles.  
Les autres objets : modèles 3D, sons, vidéos sont quand à eux sous licence Creative Commons Attribution Noncommercial : [CC-BY-NC](https://sourceforge.net/p/undead-murderer/wiki/CC-BY-NC/)  
![CC-BY-NC](https://framagit.org/Paullux/Undead_Murderer/raw/master/Images/120px-Cc-by-nc_icon.svg.png)  
  
## Animation blender qui est incorporé dans le jeu

[![Watch the video](https://framagit.org/Paullux/Undead_Murderer/raw/master/Images/PlateauYTV.png)](https://youtu.be/Xfb3c7HBlUw)  

## Capture vidéo du jeu

[![Watch the video](https://framagit.org/Paullux/undead-murderer---version-godot/raw/master/_Images/GameYTV.png)](https://youtu.be/9czZA110kBQ)  

