# scene_switcher.gd
extends Node

# Private variable
var _param1 = null
var _param2 = null
var _param3 = null

# Call this instead to be able to provide arguments to the next scene
func set_param(param1=null, param2=null, param3=null):
	_param1 = param1
	_param2 = param2
	_param3 = param3

# In the newly opened scene, you can get the parameters by name
func get_param(name):
	if _param1 != null and _param1.has(name):
		return _param1[name]
	if _param2 != null and _param2.has(name):
		return _param2[name]
	if _param3 != null and _param3.has(name):
		return _param3[name]
	return null