extends RichTextLabel


func _ready():
	set_process_input(true)
	pass

func _unhandled_input(event):
    if event is InputEventKey:
        if event.pressed and event.scancode == KEY_ESCAPE:
            get_tree().change_scene("res://_Scene/Scene_00.tscn")
