extends Sprite

onready var anim = get_node("./Ouverture des Yeux/")
onready var Sound = get_node("./RéveilMatin")

func _ready():
	anim.play("Ouverture des Yeux")
	Sound.play()
	pass

