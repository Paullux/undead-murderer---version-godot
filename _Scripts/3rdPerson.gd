extends VehicleBody

const GRAVITY = -24.8
var vel = Vector3()
const MAX_SPEED = 20
const JUMP_SPEED = 18
const ACCEL= 4.5

var dir = Vector3()


const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var rotation_helper

var MOUSE_SENSITIVITY = 0.05

onready var camera = get_node("./Rotation_Helper/Camera")
onready var Boat = get_node(".")
onready var BateauAnim = get_node("./boat01/BateauQuiTourne")
onready var myHealthLabel = get_node("./HUD/Panel/Gun_label")
onready var contaminationLabel = get_node("./HUD/Panel/Gun_label2")
onready var YoureDead = get_node("../YoureDead")
onready var LecteurVideo = get_node("./HUD/VideoPlayer")
var myHealth = 0
var contamination = 0
var VideoLue = false
var PassVideo = false

onready var Objectives = get_node("/root/WorldEnvironment/NinePatchRect")
var ObjectivesArray = ["Go to shopping groceries", "", "",""]
var newObjective = ""
var oldObjective = ""
var state

func _ready():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), script_save_load.load_game('VOLUME_MASTER'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Bruitage"), script_save_load.load_game('VOLUME_BRUITAGE'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), script_save_load.load_game('VOLUME_MUSIC'))
	rotation_helper = $Rotation_Helper
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	myHealth = scene_switcher.get_param("myHealth")
	contamination = scene_switcher.get_param("contamination")
	LecteurVideo.hide()
	set_max_contacts_reported(5)
	Objectives.show()
	pass


func _physics_process(delta):
	
	ObjectivesArray.sort()
	ObjectivesArray.invert()
	
	if ObjectivesArray[0] == "":
		Objectives.get_node("./Objective1").set_text("Not objective")
	else:
		Objectives.get_node("./Objective1").set_text(ObjectivesArray[0])
	Objectives.get_node("./Objective2").set_text(ObjectivesArray[1])
	Objectives.get_node("./Objective3").set_text(ObjectivesArray[2])
	
	if newObjective != "" and newObjective != oldObjective:
		ObjectivesArray.insert(ObjectivesArray.size(), newObjective)
		oldObjective = newObjective
	
	var bodiesHit = get_colliding_bodies()
	if !VideoLue && bodiesHit.size() > 0:
		for bodyHit in bodiesHit:
			print(bodyHit.name)
			if bodyHit.name == "Harbor":
				scene_switcher.set_param({"myHealth":myHealth, "contamination":contamination, "PreviousScene":2})
				if NiveauAtteint.load_game('LEVEL_ATTEINT') < 2:
					NiveauAtteint.save_game({LEVEL_ATTEINT = 2})
				$boat01/AudioStreamPlayer3D.stop()
				#LecteurVideo.show()
				#LecteurVideo.play()
				VideoLue = true
	if !LecteurVideo.is_playing():
		process_input(delta)
		process_movement(delta)
	if VideoLue:
		get_tree().change_scene("res://_Scene/Scene_03.tscn")
	
	if self.translation.y < -100:
		myHealth = 0
	if myHealth > 0:
		myHealth -= contamination * 0.01
	if myHealth <=0:
		myHealth = 0
		YoureDead.show()
		var t1 = Timer.new()
		t1.set_wait_time(3)
		t1.set_one_shot(true)
		self.add_child(t1)
		t1.start()
		yield(t1, "timeout")
		get_tree().change_scene("res://_Scene/Scene_00.tscn")
	
	contaminationLabel.set_text("Contamination: " + String(contamination) + "%")
	myHealthLabel.set_text("Health: " + String(round(myHealth)))

func process_input(delta):

	dir = Vector3()
	var cam_xform = camera.get_global_transform()

	var input_movement_vector = Vector2()

	if Input.is_action_pressed("Avancer"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("Reculer"):
		input_movement_vector.y -= 1
		

	input_movement_vector = input_movement_vector.normalized()

	dir -= (cam_xform.basis.z.normalized() * input_movement_vector.y)


func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()
	
	vel = Boat.set_linear_velocity(5 * Vector3(dir.x, 0, dir.z))# * Vector3(0,0,1))

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:

		var rotat = deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1)
		rotate(Vector3 (0,1,0), rotat)
		if not BateauAnim.is_playing():
			if rotat > 0.001:
				BateauAnim.play("BateauGauche")
			elif rotat < -0.001:
				BateauAnim.play("BateauDroite")
			else:
				BateauAnim.play("BateauIdle")
	
	if event is InputEventKey and event.pressed and not event.echo:
		if Input.is_action_just_pressed("Afficher Objectifs"):
			if Objectives.is_visible():
				Objectives.hide()
			else:
				Objectives.show()