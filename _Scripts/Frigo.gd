extends Spatial

onready var Porte = get_node("../../AnimationPlayer")
onready var Here = get_node(".")

var ferme = true

func _ready():
	#get_node("Area").connect("area_enter", self, "_on_collision")
	pass

#func _on_collision(value):
#	var collider = value.get_parent()
#	if ferme && collider.name == "Player":
#		Porte.play("Ouverture Frigo")
#		ferme = false
#	if !ferme && collider == 0:
#		Porte.play("Fermeture Frigo")
#		ferme = true
#	pass

func _process(delta):
	if is_colliding():
		Porte.play("Ouverture Frigo")
	else:
		Porte.play("Fermeture Frigo")
	pass