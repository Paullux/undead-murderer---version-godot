extends KinematicBody

const GRAVITY = -24.8
var vel = Vector3()
const ACCEL= 4.5

var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var camera
var rotation_helper

var MAX_SPEED = script_save_load.load_game('MAX_SPEED')
var JUMP_SPEED = script_save_load.load_game('JUMP_SPEED')
var MOUSE_SENSITIVITY = script_save_load.load_game('MOUSE_SENSITIVITY')

var _yaw = 0

var _pitch = 0

var camera_angle = 0

var myHealth = 100
var contamination = 0

var debAnim = true

onready var myHealthLabel = get_node("./HUD/Panel/Gun_label")
onready var contaminationLabel = get_node("./HUD/Panel/Gun_label2")
onready var YoureDead = get_node("../YoureDead")
onready var YoureDeadSound = get_node("../YoureDead/AudioStreamPlayeryd")
onready var YeuxSprite = get_node("../YeuxSprite")
onready var moveBoat = get_node("../boat01/Move Boat et Player")
onready var moveBoatSound = get_node("../boat01/Move Boat et Player/AudioStreamPlayer")
onready var PorteFrigo = get_node("/root/WorldEnvironment/Frigo/AnimationPlayer")
onready var FrigoVide = get_node("/root/WorldEnvironment/Frigo/AudioStreamPlayer")
onready var frigo1 = get_node("/root/WorldEnvironment/Frigo/Frigo/Box046/StaticBody")
onready var frigo2 = get_node("/root/WorldEnvironment/Frigo/Frigo/Box046")
onready var frigo3 = get_node("/root/WorldEnvironment/Frigo/Frigo")
onready var frigo4 = get_node("/root/WorldEnvironment/Frigo")
onready var frigo5 = get_node("/root/WorldEnvironment/Frigo/Object004")
onready var frigo6 = get_node("/root/WorldEnvironment/Frigo/Object004/StaticBody")
onready var ModeEmploi = get_node("/root/WorldEnvironment/ModeEmploi")
onready var allumeTVSound = get_node("./allumeTV")
onready var ouvertureFrigoSound = get_node("./ouvertureFrigo")

onready var Objectives = get_node("/root/WorldEnvironment/NinePatchRect")
var ObjectivesArray = ["", "", "",""]
var newObjective = ""
var oldObjective = ""

#onready var player = get_node("./HUD")
var YD = false
var DejaOuvert = false
export var Utiliser = false
export var activeTV = false
var tvAllume = false
var frigoOuvert = false
var AllumeTV = false
var OuvreFrigo = false



func _ready():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), script_save_load.load_game('VOLUME_MASTER'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Bruitage"), script_save_load.load_game('VOLUME_BRUITAGE'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), script_save_load.load_game('VOLUME_MUSIC'))
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper
	YeuxSprite.show()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Objectives.show()
	ModeEmploi.show()
	
func set_Create_Objective(delta):
	var tATV = Timer.new()
	tATV.set_wait_time(5)
	tATV.set_one_shot(true)
	self.add_child(tATV)
	tATV.start()
	yield(tATV, "timeout")
	ModeEmploi.hide()
	if not allumeTVSound.is_playing() and not AllumeTV:
		allumeTVSound.play(0)
		newObjective = "Turn On the TV"
		AllumeTV = true
	if tvAllume:
		var tFrigo = Timer.new()
		tFrigo.set_wait_time(8)
		tFrigo.set_one_shot(true)
		self.add_child(tFrigo)
		tFrigo.start()
		yield(tFrigo, "timeout")
		if not ouvertureFrigoSound.is_playing() and not OuvreFrigo:
			ouvertureFrigoSound.play(0)
			newObjective = "Open the Fridge"
			OuvreFrigo = true
	pass

func _process(delta):

	ObjectivesArray.sort()
	ObjectivesArray.invert()


	if ObjectivesArray[0] == "":
		Objectives.get_node("./Objective1").set_text("Not objective")
	else:
		Objectives.get_node("./Objective1").set_text(ObjectivesArray[0])
	Objectives.get_node("./Objective2").set_text(ObjectivesArray[1])
	Objectives.get_node("./Objective3").set_text(ObjectivesArray[2])

	if newObjective != "" and newObjective != oldObjective:
		ObjectivesArray.insert(ObjectivesArray.size(), newObjective)
		oldObjective = newObjective

	if self.translation.y < -100:
		myHealth = 0
	if myHealth > 0:
		myHealth -= contamination * 0.01
	if myHealth <=0:
		myHealth = 0
		if not YoureDeadSound.is_playing():
			if not YD:
				YoureDeadSound.play()
				YD = true
			YoureDead.show()
			var t1 = Timer.new()
			t1.set_wait_time(3)
			t1.set_one_shot(true)
			self.add_child(t1)
			t1.start()
			yield(t1, "timeout")
			get_tree().change_scene("res://_Scene/Scene_00.tscn")
	var coliBodies = $Area.get_overlapping_bodies()
	if coliBodies.size() > 0:
		for coliBody in coliBodies:
			if coliBody.is_in_group("Zombies"):
				if myHealth > 1:
					myHealth -= 1
				if contamination <= 99:
					contamination += 1
				else:
					contamination = 100
			if coliBody.is_in_group("boat") and debAnim and frigoOuvert and tvAllume:
				moveBoat.play("moveBoat")
				moveBoatSound.play()
				debAnim = false
			if Utiliser && coliBody.is_in_group("Frigo") && tvAllume && OuvreFrigo:
				PorteFrigo.play("Ouverture Frigo")
				var tFRIGO = Timer.new()
				tFRIGO.set_wait_time(5)
				tFRIGO.set_one_shot(true)
				self.add_child(tFRIGO)
				tFRIGO.start()
				yield(tFRIGO, "timeout")
				PorteFrigo.play("Fermeture Frigo")
				FrigoVide.play()
				DejaOuvert = false
				for i in range(0, ObjectivesArray.size()):
					if ObjectivesArray[i] == "Open the Fridge":
						ObjectivesArray[i] = "Go to shopping groceries"
				frigoOuvert = true
			if Utiliser && coliBody.is_in_group("TV") && AllumeTV:
				activeTV = true
				for i in range(0, ObjectivesArray.size()):
					if ObjectivesArray[i] == "Turn On the TV":
						ObjectivesArray[i] = ""
				tvAllume = true
	if moveBoat.is_playing():
		var t2 = Timer.new()
		t2.set_wait_time(3)
		t2.set_one_shot(true)
		self.add_child(t2)
		t2.start()
		contaminationLabel.set_text("Contamination: " + String(contamination) + "%")
		myHealthLabel.set_text("Health: " + String(round(myHealth)))
		yield(t2, "timeout")
		scene_switcher.set_param({"myHealth":myHealth, "contamination":contamination, "PreviousScene":1})
		if NiveauAtteint.load_game('LEVEL_ATTEINT') < 1:
			NiveauAtteint.save_game({LEVEL_ATTEINT = 1})
		get_tree().change_scene("res://_Scene/Scene_02.tscn")
	contaminationLabel.set_text("Contamination: " + String(contamination) + "%")
	myHealthLabel.set_text("Health: " + String(round(myHealth)))
	pass

func _physics_process(delta):
	process_input(delta)
	process_movement(delta)
	set_Create_Objective(delta)

func process_input(delta):
	# Walking
	dir = Vector3()
	var cam_xform = camera.get_global_transform()

	var input_movement_vector = Vector2()

	if Input.is_action_pressed("Avancer"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("Reculer"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("Aller a gauche"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("Aller a droite"):
		input_movement_vector.x += 1

	input_movement_vector = input_movement_vector.normalized()

	dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	dir += cam_xform.basis.x.normalized() * input_movement_vector.x
	# ----------------------------------

	# ----------------------------------
	# Jumping
	if is_on_floor():
		if Input.is_action_just_pressed("Sauter"):
			vel.y = JUMP_SPEED
	# ----------------------------------

	# ----------------------------------
	# Capturing/Freeing the cursor
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	# ----------------------------------

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()

	vel.y += delta*GRAVITY

	var hvel = vel
	hvel.y = 0

	var target = dir
	target *= MAX_SPEED

	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel,Vector3(0,1,0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))


func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:

		var rotat = deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1)
		rotate(Vector3 (0,1,0), rotat)

		var camRotat = deg2rad(event.relative.y * MOUSE_SENSITIVITY)
		rotation_helper.rotate(Vector3 (1,0,0), camRotat)

	if event is InputEventKey and event.pressed and not event.echo:
		if Input.is_action_just_pressed("Utiliser"):
			Utiliser = true
		else:
			Utiliser = false
		if Input.is_action_just_pressed("Afficher Objectifs"):
			if Objectives.is_visible():
				Objectives.hide()
			else:
				Objectives.show()
