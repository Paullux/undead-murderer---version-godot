extends KinematicBody

const GRAVITY = -24.8
var vel = Vector3()
const MAX_SPEED = 20
const JUMP_SPEED = 18
const ACCEL= 4.5

var dir = Vector3()


const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var rotation_helper

var MOUSE_SENSITIVITY = 0.05

onready var camera = get_node("./Heros_1/Camera")
onready var Player = get_node(".")
onready var myHealthLabel = get_node("../HUD/Panel/Gun_label")
onready var contaminationLabel = get_node("../HUD/Panel/Gun_label2")
onready var YoureDead = get_node("../YoureDead")
onready var AnimPlayer = get_node("AnimationPlayer")
var myHealth = 0
var contamination = 0
var PreviousScene = 0
var VideoLue = false
var PassVideo = false

onready var Objectives = get_node("/root/Spatial/NinePatchRect")
var ObjectivesArray = ["Go to shopping groceries", "", "",""]
var newObjective = ""
var oldObjective = ""
var state
var First = false
var First2 = false
var touchMurs = false
var veloy = 50

func _ready():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), script_save_load.load_game('VOLUME_MASTER'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Bruitage"), script_save_load.load_game('VOLUME_BRUITAGE'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), script_save_load.load_game('VOLUME_MUSIC'))
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	myHealth = scene_switcher.get_param("myHealth")
	contamination = scene_switcher.get_param("contamination")
	PreviousScene = scene_switcher.get_param("PreviousScene")
	if PreviousScene == 4:
		var ObjectivesArray = ["Do your shopping", "", "",""]
		get_node(".").translation = Vector3(31.55,2.128,-34.327)
	Objectives.show()
	pass


func _physics_process(delta):

	ObjectivesArray.sort()
	ObjectivesArray.invert()

	if not AnimPlayer.is_playing():
		AnimPlayer.play("Heros_1_Idle")
	
	if ObjectivesArray[0] == "":
		Objectives.get_node("./Objective1").set_text("Not objective")
	else:
		Objectives.get_node("./Objective1").set_text(ObjectivesArray[0])
	Objectives.get_node("./Objective2").set_text(ObjectivesArray[1])
	Objectives.get_node("./Objective3").set_text(ObjectivesArray[2])

	if newObjective != "" and newObjective != oldObjective:
		ObjectivesArray.insert(ObjectivesArray.size(), newObjective)
		oldObjective = newObjective

	var coliBodies = $Area.get_overlapping_bodies()
	if coliBodies.size() > 0:
		for coliBody in coliBodies:
			print(coliBody.name)
			if coliBody.name == "PortedeSortie":
				scene_switcher.set_param({"myHealth":myHealth, "contamination":contamination, "PreviousScene":2})
				if NiveauAtteint.load_game('LEVEL_ATTEINT') < 3:
					NiveauAtteint.save_game({LEVEL_ATTEINT = 3})
				get_tree().change_scene("res://_Scene/Scene_04.tscn")
			if coliBody.name == "Battements":
				$AudioStreamPlayer.play()

	if self.translation.y < -100:
		myHealth = 0
	if myHealth > 0:
		myHealth -= contamination * 0.01
	if myHealth <=0:
		myHealth = 0
		YoureDead.show()
		var t1 = Timer.new()
		t1.set_wait_time(3)
		t1.set_one_shot(true)
		self.add_child(t1)
		t1.start()
		yield(t1, "timeout")
		get_tree().change_scene("res://_Scene/Scene_00.tscn")

	contaminationLabel.set_text("Contamination: " + String(contamination) + "%")
	myHealthLabel.set_text("Health: " + String(round(myHealth)))
	process_input(delta)
	process_movement(delta)

func process_input(delta):

	dir = Vector3()
	var cam_xform = camera.get_global_transform()

	var input_movement_vector = Vector2()

	if Input.is_action_pressed("Avancer"):
		input_movement_vector.y += 1
		AnimPlayer.play("Heros_1_Marche")
		First2 = true
	if Input.is_action_pressed("Reculer"):
		input_movement_vector.y -= 1
		AnimPlayer.play("Heros_1_Marche")
		First2 = true
	if Input.is_action_pressed("Aller a gauche"):
		input_movement_vector.x -= 1
		AnimPlayer.play("Heros_1_Marche")
		First2 = true
	if Input.is_action_pressed("Aller a droite"):
		input_movement_vector.x = 1
		AnimPlayer.play("Heros_1_Marche")
		First2 = true
	if First2 && not Input.is_action_pressed("Avancer") && not Input.is_action_pressed("Reculer") && not Input.is_action_pressed("Aller a gauche") && not Input.is_action_pressed("Aller a droite"):
		AnimPlayer.play("Heros_1_Idle")
		First2 = false

	input_movement_vector = input_movement_vector.normalized()

	dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	dir += cam_xform.basis.x.normalized() * input_movement_vector.x
    # ----------------------------------

    # ----------------------------------
    # Jumping
	if is_on_floor():
		veloy = 50
		if Input.is_action_just_pressed("Sauter"):
			veloy = 20
			vel.y = JUMP_SPEED*10
			AnimPlayer.play("Heros_1_Saut1")
		if Input.is_action_just_pressed("Sauter") && First:
				AnimPlayer.play("Heros_1_Saut2")
				First = false
		First = true
	elif not is_on_wall():
		var t = Timer.new()
		t.set_wait_time(.75)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		if not is_on_floor() and not is_on_wall():
			AnimPlayer.play("Heros_1_falldown")


	input_movement_vector = input_movement_vector.normalized()

	dir -= (cam_xform.basis.z.normalized() * input_movement_vector.y)


func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()

	vel.y += delta*GRAVITY*veloy

	var hvel = vel
	hvel.y = 0

	var target = dir
	target *= MAX_SPEED

	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(.5*vel,Vector3(0,1,0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:

		var rotat = deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1)
		rotate(Vector3 (0,1,0), rotat)

	if event is InputEventKey and event.pressed and not event.echo:
		if Input.is_action_just_pressed("Afficher Objectifs"):
			if Objectives.is_visible():
				Objectives.hide()
			else:
				Objectives.show()