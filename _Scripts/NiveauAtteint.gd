extends Node

var ContaSet = 0
var savegameFile = File.new()
var path = "user://NiveauAttendsFile.save"

var Dict = {LEVEL_ATTEINT = 0}

func save_game(Dictnew):
    savegameFile.open(path, File.WRITE)
    savegameFile.store_line(to_json(Dictnew))
    savegameFile.close()

func load_game(name):
    if savegameFile.file_exists(path):
        savegameFile.open(path, File.READ)
        Dict = parse_json(savegameFile.get_line())
        savegameFile.close()
    else:
        savegameFile.open(path, File.WRITE)
        savegameFile.store_line(to_json(Dict))
        savegameFile.close()
    return Dict[name]