extends KinematicBody

const GRAVITY = -24.8
var vel = Vector3()

const ACCEL= 4.5

var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

const ray_length = 10000

var camera
var rotation_helper

var MAX_SPEED = script_save_load.load_game('MAX_SPEED')
var JUMP_SPEED = script_save_load.load_game('JUMP_SPEED')
var MOUSE_SENSITIVITY = script_save_load.load_game('MOUSE_SENSITIVITY')

var _yaw = 0

var _pitch = 0

var camera_angle = 0

var myHealth = 0
var contamination = 0
var PreviousScene = 0

var debAnim = true

onready var myHealthLabel = get_node("./HUD/Panel/Gun_label")
onready var contaminationLabel = get_node("./HUD/Panel/Gun_label2")
onready var YoureDead = get_node("../YoureDead")
onready var YoureDeadSound = get_node("../YoureDead/AudioStreamPlayeryd")
onready var PorteSortieSound = get_node("/root/WorldEnvironment/SuperMarche/Armature/Skeleton/PorteArriere/AudioStreamPlayer")
onready var PorteSortieAnim = get_node("/root/WorldEnvironment/SuperMarche/AnimationPlayer")
onready var PorteFermeeSound = get_node("/root/WorldEnvironment/SuperMarche/AudioStreamPlayer")
onready var Magasin = get_node("/root/WorldEnvironment/SuperMarche")
onready var EntreeMagasinSound = get_node("/root/WorldEnvironment/SuperMarche/AudioStreamPlayer2")
onready var mycam = get_node("/root/WorldEnvironment/Player/Rotation_Helper/Camera")
#onready var player = get_node("./HUD")
var YD = false
var magasinVu = false

onready var Objectives = get_node("/root/WorldEnvironment/NinePatchRect")
var ObjectivesArray = ["Go to shopping groceries", "", "",""]
var newObjective = ""
var oldObjective = ""
export var Utiliser = false
var PorteFerm = true
var canChanged = true

func _ready():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), script_save_load.load_game('VOLUME_MASTER'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Bruitage"), script_save_load.load_game('VOLUME_BRUITAGE'))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), script_save_load.load_game('VOLUME_MUSIC'))
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	myHealth = scene_switcher.get_param("myHealth")
	contamination = scene_switcher.get_param("contamination")
	PreviousScene = scene_switcher.get_param("PreviousScene")
	if PreviousScene == 4:
		var ObjectivesArray = ["Do your shopping", "", "",""]
		get_node(".").translation = Vector3(31.55,2.128,-34.327)

func _process(delta):

	ObjectivesArray.sort()
	ObjectivesArray.invert()

	if ObjectivesArray[0] == "":
		Objectives.get_node("./Objective1").set_text("Not objective")
	else:
		Objectives.get_node("./Objective1").set_text(ObjectivesArray[0])
	Objectives.get_node("./Objective2").set_text(ObjectivesArray[1])
	Objectives.get_node("./Objective3").set_text(ObjectivesArray[2])

	if newObjective != "" and newObjective != oldObjective:
		ObjectivesArray.insert(ObjectivesArray.size(), newObjective)
		oldObjective = newObjective

	if self.translation.y < -100:
		myHealth = 0
	if myHealth > 0:
		myHealth -= contamination * 0.01
	if myHealth <=0:
		myHealth = 0
		if not YoureDeadSound.is_playing():
			if not YD:
				YoureDeadSound.play()
				YD = true
			YoureDead.show()
			var t1 = Timer.new()
			t1.set_wait_time(3)
			t1.set_one_shot(true)
			self.add_child(t1)
			t1.start()
			yield(t1, "timeout")
			get_tree().change_scene("res://_Scene/Scene_00.tscn")
	var coliBodies = $Area.get_overlapping_bodies()
	if coliBodies.size() > 0:
		for coliBody in coliBodies:
			if coliBody.is_in_group("Zombies"):
				if myHealth > 1:
					myHealth -= 1
				if contamination <= 99:
					contamination += 1
				else:
					contamination = 100
			if coliBody.is_in_group("Barricade"):
				if myHealth > 1:
					myHealth -= .1
			if coliBody.is_in_group("PorteFermee") and Utiliser:
				if not PorteFermeeSound.is_playing():
					PorteFermeeSound.play()
			if coliBody.is_in_group("PorteOuverte"):
				if PorteFerm:
					if Utiliser:
						PorteSortieAnim.play("ArmatureAction")
						PorteFerm = false
						PorteSortieSound.play()
				if not PorteFerm and not PorteSortieAnim.is_playing():
					var tPorte = Timer.new()
					tPorte.set_wait_time(4)
					tPorte.set_one_shot(true)
					self.add_child(tPorte)
					tPorte.start()
					yield(tPorte, "timeout")
					scene_switcher.set_param({"myHealth":myHealth, "contamination":contamination, "PreviousScene":3})
					if NiveauAtteint.load_game('LEVEL_ATTEINT') < 4:
						NiveauAtteint.save_game({LEVEL_ATTEINT = 4})
					get_tree().change_scene("res://_Scene/Scene_05.tscn")
	contaminationLabel.set_text("Contamination: " + String(contamination) + "%")
	myHealthLabel.set_text("Health: " + String(round(myHealth)))
	pass

func _physics_process(delta):
    process_input(delta)
    process_movement(delta)

func process_input(delta):

    # ----------------------------------
    # Walking
	dir = Vector3()
	var cam_xform = camera.get_global_transform()

	var input_movement_vector = Vector2()

	if Input.is_action_pressed("Avancer"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("Reculer"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("Aller a gauche"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("Aller a droite"):
		input_movement_vector.x = 1

	input_movement_vector = input_movement_vector.normalized()

	dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	dir += cam_xform.basis.x.normalized() * input_movement_vector.x
    # ----------------------------------

    # ----------------------------------
    # Jumping
	if is_on_floor():
		if Input.is_action_just_pressed("Sauter"):
			vel.y = JUMP_SPEED
    # ----------------------------------

    # ----------------------------------
    # Capturing/Freeing the cursor
	#if Input.is_action_just_pressed("ui_cancel"):
	#	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
	#		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	#	else:
	#		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    # ----------------------------------

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()

	vel.y += delta*GRAVITY

	var hvel = vel
	hvel.y = 0

	var target = dir
	target *= MAX_SPEED

	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel,Vector3(0,1,0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))



func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:

		var rotat = deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1)
		rotate(Vector3 (0,1,0), rotat)

		var camRotat = deg2rad(event.relative.y * MOUSE_SENSITIVITY)
		rotation_helper.rotate(Vector3 (1,0,0), camRotat)
	
		var mycamera = $Rotation_Helper/Camera
		var from = mycamera.project_ray_origin(event.position)
		var to = from + mycamera.project_ray_normal(event.position) * ray_length
		var directState = PhysicsServer.space_get_direct_state(mycamera.get_world().get_space())
		var result = directState.intersect_ray(from, to, [self])
		if result.has("collider"):
			var hit_result = result["collider"]
			if hit_result.is_in_group("SuperMarche") and not magasinVu:
				EntreeMagasinSound.play()
				newObjective = "Enter the store"
				magasinVu = true
				
	if event is InputEventKey and event.pressed and not event.echo:
		if Input.is_action_just_pressed("Utiliser"):
			Utiliser = true
		else:
			Utiliser = false
		if Input.is_action_just_pressed("Afficher Objectifs"):
			if Objectives.is_visible():
				Objectives.hide()
			else:
				Objectives.show()
