extends WorldEnvironment

var mat_skin_male = ResourceLoader.load("res://_ZombieHomme1-v2/Mon_deuxième_avatar_Body_Old_caucasian_male_002.material")
var old_skin_male = ImageTexture.new()
var new_skin_male = ImageTexture.new()
var mat_wear_male = ResourceLoader.load("res://_ZombieHomme1-v2/Mon_deuxième_avatar_Male_casualsuit06_Male_casualsuit06_002.material")
var old_wear_male = ImageTexture.new()
var new_wear_male = ImageTexture.new()
var mat_skin_female = ResourceLoader.load("res://_ZombieFemme1/Premier_avatar_3_Female_muscle_13442_Old_caucasian_female.material")
var old_skin_female = ImageTexture.new()
var new_skin_female = ImageTexture.new()
var mat_wear_female = ResourceLoader.load("res://_ZombieFemme1/Premier_avatar_3_Female_sportsuit01_Female_sportsuit01.material")
var old_wear_female = ImageTexture.new()
var new_wear_female = ImageTexture.new()
var mat_sky = ResourceLoader.load("res://_Scene/Scene_04.tscn::1")
var old_sky = ImageTexture.new()
var new_sky = ImageTexture.new()
var rock_mat = ResourceLoader.load("res://_incity1/Material_001.material")
var old_rock = Color("#3c4248")
var new_rock = Color("#e2f2fe")

var tClair1
var tClair2
var tClair3

func _ready():
	$Thunder.hide()
	old_skin_male = preload("res://_ZombieHomme1-v2/images/old_lightskinned_male_diffuse-V2.png")
	new_skin_male = preload("res://_Textures_avatars_de_base/old_lightskinned_male_diffuse.png")
	old_wear_male = preload("res://_ZombieHomme1-v2/images/male_casualsuit06_diffuse.png")
	new_wear_male = preload("res://_Textures_avatars_de_base/male_casualsuit06_diffuse.png")
	old_skin_female = preload("res://_ZombieFemme1/images/female_Zombie1 skin.png")
	new_skin_female = preload("res://_Textures_avatars_de_base/old_lightskinned_female_diffuse.png")
	old_wear_female = preload("res://_ZombieFemme1/images/female_Zombie1 clothes.png")
	new_wear_female = preload("res://_Textures_avatars_de_base/female_sportsuit01_diffuse.png")
	old_sky = preload("res://skydome pack 4/sky4.png")
	new_sky = preload("res://skydome pack 4/sky4(1).png")
	tClair1 = Timer.new()
	tClair1.connect("timeout", self, "_on_reality")
	tClair1.set_wait_time(15)
	tClair1.set_one_shot(false)
	self.add_child(tClair1)
	tClair1.start()
	pass

func _process(delta):
	if tClair1.is_stopped():
		tClair3.start()
	pass

func _on_reality():
	
	mat_skin_male.albedo_texture = new_skin_male
	mat_wear_male.albedo_texture = new_wear_male
	mat_skin_female.albedo_texture = new_skin_female
	mat_wear_female.albedo_texture = new_wear_female
	rock_mat.albedo_color = new_rock
	mat_sky.set_panorama(new_sky)
	
	$AudioStreamPlayer.play()
	$Thunder.show()
	
	var tClair2 = Timer.new()
	tClair2.set_wait_time(.15)
	tClair2.set_one_shot(false)
	self.add_child(tClair2)
	tClair2.start()
	yield(tClair2, "timeout")
	
	$Thunder.hide()
	
	tClair3 = Timer.new()
	tClair3.set_wait_time(.25)
	tClair3.set_one_shot(false)
	self.add_child(tClair3)
	tClair3.start()
	yield(tClair3, "timeout")
	
	$AudioStreamPlayer.play()
	$Thunder.show()
	
	var tClair4 = Timer.new()
	tClair4.set_wait_time(.15)
	tClair4.set_one_shot(false)
	self.add_child(tClair4)
	tClair4.start()
	yield(tClair4, "timeout")
	
	$Thunder.hide()
	mat_skin_male.albedo_texture = old_skin_male
	mat_wear_male.albedo_texture = old_wear_male
	mat_skin_female.albedo_texture = old_skin_female
	mat_wear_female.albedo_texture = old_wear_female
	rock_mat.albedo_color = old_rock
	mat_sky.set_panorama(old_sky)
	
	pass