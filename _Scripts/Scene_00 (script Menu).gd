extends Spatial

onready var GreyScale = get_node("./Control/GreyScale")
onready var niveau1 = get_node("./Control/VBoxContainer/niveau1")
onready var niveau2 = get_node("./Control/VBoxContainer/niveau2")
onready var niveau3 = get_node("./Control/VBoxContainer/niveau3")
onready var niveau4 = get_node("./Control/VBoxContainer/niveau4")
onready var niveau5 = get_node("./Control/VBoxContainer/niveau5")
onready var Musique = get_node("./AudioStreamPlayer3D")
var niveauAtteint;

func _ready():
	Musique.play()
	get_node("./Control/GreyScale/HSlider").value = script_save_load.load_game('MAX_SPEED')
	get_node("./Control/GreyScale/HSlider2").value = script_save_load.load_game('JUMP_SPEED')
	get_node("./Control/GreyScale/HSlider3").value = script_save_load.load_game('MOUSE_SENSITIVITY')
	get_node("./Control/GreyScale/VSlider4").value = script_save_load.load_game('VOLUME_MASTER')
	get_node("./Control/GreyScale/VSlider5").value = script_save_load.load_game('VOLUME_BRUITAGE')
	get_node("./Control/GreyScale/VSlider6").value = script_save_load.load_game('VOLUME_MUSIC')
	niveauAtteint = NiveauAtteint.load_game('LEVEL_ATTEINT')
	if niveauAtteint < 1:
		niveau2.hide()
	if niveauAtteint < 2:
		niveau3.hide()
	if niveauAtteint < 3:
		niveau4.hide()
	if niveauAtteint < 4:
		niveau5.hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	set_process_input(true)
	pass

func _process(delta):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), get_node("./Control/GreyScale/VSlider4").get("value"))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Bruitage"), get_node("./Control/GreyScale/VSlider5").get("value"))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), get_node("./Control/GreyScale/VSlider6").get("value"))
	pass

func _unhandled_input(event):
    if event is InputEventKey:
        if event.pressed and event.scancode == KEY_ESCAPE:
            get_tree().quit()

func _on_niveau1_pressed():
	get_tree().change_scene("res://_Scene/Scene_01.tscn")
	pass # replace with function body

func _on_niveau2_pressed():
	scene_switcher.set_param({"myHealth":100, "contamination":0, "PreviousScene":0})
	get_tree().change_scene("res://_Scene/Scene_02.tscn")
	pass # replace with function body
	
func _on_niveau3_pressed():
	scene_switcher.set_param({"myHealth":100, "contamination":0, "PreviousScene":0})
	get_tree().change_scene("res://_Scene/Scene_03.tscn")
	pass # replace with function body

func _on_niveau4_pressed():
	scene_switcher.set_param({"myHealth":100, "contamination":0, "PreviousScene":0})
	get_tree().change_scene("res://_Scene/Scene_04.tscn")
	pass # replace with function body

func _on_niveau5_pressed():
	scene_switcher.set_param({"myHealth":100, "contamination":0, "PreviousScene":0})
	get_tree().change_scene("res://_Scene/Scene_05.tscn")
	pass # replace with function body

func _on_Options_pressed():
	if GreyScale.is_visible():
		script_save_load.save_game({MAX_SPEED = get_node("./Control/GreyScale/HSlider").get("value"), JUMP_SPEED = get_node("./Control/GreyScale/HSlider2").get("value"), MOUSE_SENSITIVITY = get_node("./Control/GreyScale/HSlider3").get("value"), VOLUME_MASTER = get_node("./Control/GreyScale/VSlider4").get("value"),VOLUME_BRUITAGE = get_node("./Control/GreyScale/VSlider5").get("value"), VOLUME_MUSIC = get_node("./Control/GreyScale/VSlider6").get("value")})
		GreyScale.hide()
		niveau1.show()
		if niveauAtteint >= 1:
			niveau2.show()
		if niveauAtteint >= 2:
			niveau3.show()
		if niveauAtteint >= 3:
			niveau4.show()
	else:
		niveau1.hide()
		niveau2.hide()
		niveau3.hide()
		niveau4.hide()
		GreyScale.show()
	pass

func _on_SiteWeb_pressed():
	OS.shell_open("https://www.undead-murderer.com/")
	get_tree().quit()
	pass

func _on_Forum_pressed():
	OS.shell_open("https://www.undead-murderer.com/community/")
	get_tree().quit()
	pass

func _on_Discord_pressed():
	OS.shell_open("https://discord.gg/9TFvQj")
	get_tree().quit()
	pass