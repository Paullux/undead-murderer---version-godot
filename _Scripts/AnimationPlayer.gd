extends AnimationPlayer

onready var Anim = get_node(".")

func _ready():
	Anim.play("default")
	pass

func _process(delta):
	if not Anim.is_playing():
		Anim.play("default")
	pass
