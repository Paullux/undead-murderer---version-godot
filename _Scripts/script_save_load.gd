extends Node

var ContaSet = 0
var savegameFile = File.new()
var path = "user://savegameFile-V3.save"

var Dict = {
    MAX_SPEED = 10,
    JUMP_SPEED = 10,
    MOUSE_SENSITIVITY = 0.02,
	VOLUME_MASTER = 0,
	VOLUME_BRUITAGE = 0,
	VOLUME_MUSIC = 0,
}

func save_game(Dictnew):
    savegameFile.open(path, File.WRITE)
    savegameFile.store_line(to_json(Dictnew))
    savegameFile.close()

func load_game(name):
    if savegameFile.file_exists(path):
        savegameFile.open(path, File.READ)
        Dict = parse_json(savegameFile.get_line())
        savegameFile.close()
    else:
        savegameFile.open(path, File.WRITE)
        savegameFile.store_line(to_json(Dict))
        savegameFile.close()
    return Dict[name]