extends KinematicBody

onready var anim = get_node("./MeshInstance/Marche2")

const GRAVITY = -9.81
var vel = Vector3()
const MAX_SPEED = 1
const JUMP_SPEED = 18
const ACCEL= 4.5

var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var value = 0;
var lookTarget = "../Player";
var lookPos
var vectAle = Vector3(0,0,0)

func _ready():
	anim.play("default")
	set_process(true)
	pass

func _physics_process(delta):
    process_movement(delta)

func _process(delta):
	if (not anim.is_playing()):
		anim.play("default")
		vectAle = Vector3(rand_range(0,1),0,rand_range(0,1))
	pass
	
func process_movement(delta):
	
	lookTarget = "../Player"
	
	var distance = $MeshInstance.get_global_transform().origin.distance_to(get_node(lookTarget).get_global_transform().origin)
	
	if distance < 5:
	

	
		lookPos = get_node(lookTarget).get_transform().origin
	
		var cible = (get_node(lookTarget).get_transform().origin - get_transform().origin).normalized()
	
		look_at(lookPos,Vector3(0,1,0))
	
		dir.y = 0
		dir = cible.normalized()
	
		vel.y += delta*GRAVITY

		var hvel = vel
		hvel.y = 0

		var target = dir
		target *= MAX_SPEED

		var accel
		if dir.dot(hvel) > 0:
			accel = ACCEL
		else:
			accel = DEACCEL

		hvel = hvel.linear_interpolate(target, accel*delta)
		vel.x = hvel.x
		vel.z = hvel.z
		vel = move_and_slide(vel,Vector3(0,1,0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
	
	elif distance < 20:
		
		dir.y = 0
		
		
		dir = vectAle.normalized()
		
		var nextPoint = get_global_transform().origin + Vector3(vel.x,0,vel.z)
		
		look_at(nextPoint, Vector3(0, 1, 0))
		
		vel.y += delta*GRAVITY

		var hvel = vel
		hvel.y = 0

		var target = dir
		target *= MAX_SPEED

		var accel
		if dir.dot(hvel) > 0:
			accel = ACCEL
		else:
			accel = DEACCEL

		hvel = hvel.linear_interpolate(target, accel*delta)
		vel.x = hvel.x
		vel.z = hvel.z
		vel = move_and_slide(vel,Vector3(0,1,0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
		
		#anim.stop()
	
	pass