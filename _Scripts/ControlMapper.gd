extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	InputMap.erase_action ("movement_forward")
	InputMap.erase_action ("movement_backward")
	InputMap.erase_action ("movement_left")
	InputMap.erase_action ("movement_right")
	InputMap.erase_action ("movement_jump")
	InputMap.erase_action ("_in_game_use")
	InputMap.erase_action ("_in_game_display_objectives")
	InputMap.add_action("movement_forward")
	InputMap.add_action ("movement_backward")
	InputMap.add_action ("movement_left")
	InputMap.add_action ("movement_right")
	InputMap.add_action ("movement_jump")
	InputMap.add_action ("_in_game_use")
	InputMap.add_action ("_in_game_display_objectives")
	var evF = InputEventKey.new()
	var evB = InputEventKey.new()
	var evL = InputEventKey.new()
	var evR = InputEventKey.new()
	var evJ = InputEventKey.new()
	var evU = InputEventKey.new()
	var evT = InputEventKey.new()
	evF.scancode = KEY_Z
	evB.scancode = KEY_S
	evL.scancode = KEY_Q
	evR.scancode = KEY_D
	evJ.scancode = KEY_SPACE
	evU.scancode = KEY_E
	evT.scancode = KEY_TAB
	InputMap.action_add_event ("movement_forward", evF)
	InputMap.action_add_event ("movement_backward", evB)
	InputMap.action_add_event ("movement_left", evL)
	InputMap.action_add_event ("movement_right", evR )
	InputMap.action_add_event ("movement_jump", evJ)
	InputMap.action_add_event ("_in_game_use", evU)
	InputMap.action_add_event ("_in_game_display_objectives", evT)
	pass

func _process(delta):
	
	
	pass
