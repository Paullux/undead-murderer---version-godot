extends Sprite3D

var stream = preload("res://_Videos/UndeadMurdererTV.ogv")
var neigestream = preload("res://_Videos/neigesurTVHD.ogv")
var connerieTV = preload("res://_Sounds/ConnerieTV.ogg")
var dejaLu = false
var player = VideoPlayer.new()
onready var imageDebut = get_node("../../../TextureRect")
onready var audio = get_node("./AudioStreamPlayer")
var activeTV
var Utiliser
var peutChanger = true

func _ready():
	
	imageDebut.show()
	
	var t = Timer.new()
	t.set_wait_time(3)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	
	imageDebut.hide()
	
	player.bus = "Bruitage"
	
	add_child(player)
	
	var textureVideo = player.get_video_texture()
	
	set_texture(textureVideo)
	
	player.set_stream(neigestream)
	
	set_process(true)
	pass

func _process(delta):
	
	activeTV = get_node("/root/WorldEnvironment/Player").activeTV
	Utiliser = get_node("/root/WorldEnvironment/Player").Utiliser
	
	
	var textureVideo = player.get_video_texture()
	
	set_texture(textureVideo)
	
	var isPlaying = player.is_playing()
	if Utiliser:
		if activeTV and peutChanger:
			peutChanger = false
			player.stop()
			audio.stop()
			if not dejaLu:
				player.set_stream(stream)
				audio.play(0)
			else:
				player.set_stream(neigestream)
			dejaLu = !dejaLu
	else:
		peutChanger = true
	
	activeTV = false
	get_node("/root/WorldEnvironment/Player").activeTV = false
	get_node("/root/WorldEnvironment/Player").Utiliser = false
	
	if not isPlaying:
		player.play()
	
	pass