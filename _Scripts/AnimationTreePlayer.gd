extends AnimationTree

var playback : AnimationNodeStateMachinePlayback
var first = false

func _ready():
	playback = get("parameters/playback")
	playback.start("Heros_1_Idle")
	active = true
	pass
	
func _process(delta):
	if (Input.is_action_pressed("Avancer") || Input.is_action_pressed("Reculer") || Input.is_action_pressed("Aller a gauche") || Input.is_action_pressed("Aller a droite")):
		playback.travel("Heros_1_Marche")
	else:
		playback.travel("Heros_1_Idle")
	if Input.is_action_pressed("Sauter"):
		playback.travel("Heros_1_Saut1")
		first = true
	if (Input.is_action_just_pressed("Sauter") && first):
		playback.travel("Heros_1_Saut2")
		if not Input.is_action_pressed("Sauter"):
			first = false
	pass