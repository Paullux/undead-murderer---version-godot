extends Spatial

onready var anim = get_node("./AnimationPlayer")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	if !anim.is_playing():
		anim.play("Rotation Light")
	pass
